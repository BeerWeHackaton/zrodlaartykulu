﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace ConsoleApp2
{
    class Program
    {
        static String[] sourcesDictionary = { "source", "origin", "źródło", "źródła", "tekst oryginalny", "oryginał", "Ursprung",
                                            "origine", "πηγή", "de origen", "fuente", "šaltinis"};

        static public int licznikzrodel;
        static public int ogolnylicznikzrodel;

        public static String[] hrefValue = {"\0" };
        public static String[] adresy = {"\0" };

        static void SourceFound(String adres) {
            HtmlWeb web = new HtmlWeb();
            HtmlDocument document = web.Load(adres);
            hrefValue[0] = "\0";
            for (var i = 0; i < sourcesDictionary.Length; i++)
            {
                licznikzrodel = 0;
                foreach (HtmlNode link in document.DocumentNode.SelectNodes(".//a[@href]"))
                {
                    if (link.ParentNode.InnerHtml.Contains(sourcesDictionary[i]))
                    {
                        if (document.DocumentNode.InnerHtml.ToString().Contains(sourcesDictionary[i]))
                        {
                            hrefValue[licznikzrodel] = link.GetAttributeValue("href", string.Empty);
                            licznikzrodel++;
                        }
                    }
                }
                adresy = hrefValue;
            }
        }

        static void Main(string[] args)
        {

            System.Console.WriteLine(" ------- Poszukiwanie źródeł stron WWW v1.0.0. ------- ");
            System.Console.WriteLine(" ---------------- Autor: Paweł Kuraś ----------------- ");
            System.Console.WriteLine("");
            System.Console.WriteLine("Podaj link dokumentu dla którego chcesz zbadać pochodzenie: \n");
            var poziom_zrodla = 1;
            var completeadres = "";
            var adresHTML = System.Console.ReadLine();
            if (!adresHTML.Contains("http://"))
            { completeadres = "http://" + adresHTML; }
            else
            { completeadres = adresHTML; }
            System.Console.WriteLine("Trwa przeszukiwanie dokumentu w poszukiwaniu źródeł... \n");
            adresy[0] = completeadres;

            do
            {
                for (var x=0; x < adresy.Length; x++)
                {
               SourceFound(adresy[x]);
                if(hrefValue[0]!="/0")
                {
                    for (var j = 0; j <= licznikzrodel; j++)
                    {
                        for (var i = 0; i <= poziom_zrodla; i++)
                        {
                        Console.Write(" "); }
                        Console.Write(hrefValue[j]);
                            ogolnylicznikzrodel++;
                    }
                    }
                }
                for (var z = 0; z < adresy.Length; z++)
                {
                    adresy[z] = "\0";
                    hrefValue[z] = "\0";
                }
                    poziom_zrodla++;
            }
            while (licznikzrodel != 0);

            Console.WriteLine("\n\nLiczba wszystkich źródeł artykułu: {0}", ogolnylicznikzrodel);
            Console.ReadLine();
        }


    }
}
